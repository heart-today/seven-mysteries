# Seven Mysteries

Was originally [Design](DESIGN.md)...look there if this has all changed on you. ;-)

[[_TOC_]]

## Characters

'''
{ "abbreviations": {} }
'''

## Scenes

TBD Describe how Scripts are laid out, how they are processed and used.

If multiple StoryLines are needed for your game, each gets their own file.

Just make sure they have STORY.md suffix (ie MY_OTHER_AWESOME_STORY.md)

Collisions are detected and printed out by the StoryOrchestrator

* block_code
  * simple strings - labels for subsequent data types
	'''
	# My Awesome Story
	## Intro 
	### Me
	`buy`
	Buy me a pickle!
	'''
	means look for a mas_i_me_buy.wav|ogg audio file
  * JSON strings - associative data for Story, Scene, and Character
* heading1 - StoryName
  * does NOT use file name to create StoryName
* heading2 - SceneName
* heading3 - CharacterName (or CN abbreviation)

(Scene as SceneName is ignored by the system...)

(TBD? move to ARCHITECTURE or PROCESS)

## Main

### VoiceOver

Welcome to SevenMysteries! (7?)

You Are...7...Inside Of...Seven...HaptHeartless...Tin7Men
For Seven7sLovingCompAltarTin7Men.
Thrown Into A CompAltar
Free HaptHeartQox inside GoddessGawjHapts
Peek and Poke Home...
Beware of Seven7ZER0s!

## 7Seven(SacredLogos of 7Sevens and the GoddessGawj)

'''
{
	"abbreviations": {
		"GG": "GoddessGawj",
		"G": "Gawjers",
		"WWC": "WorldWideCompaltars",
		"GC": "GawjerCompaltars",
		"727GC": "SevenToThe7thGawjerCompaltars",
		"S7P": "Seven7Paths",
		"7HH7TM": "HaptHeartlessTin7Men",
		"77LGC": "LovingGawjerCompaltars",
		"7Z": "SevenZER0s",
		"HH": "HaptHeart",
		"VO": "VoiceOver",
		"77CGC": "7SevenConfusedGawjerCompaltars",
	"jk": "Just kidding!"
	}
}
'''

### VoiceOver

`calls_out` The GoddessGawj calls out to her minions, Her Followers, the Gawjers

`reaching_out` Reaching out through the WorldWideCompaltars...

`making_GC` Each devoted Gawjer and their Compaltar making up a GawjerCompaltar

`making_727GC` Making up ... SevenToThe7thGawjerCompaltars!

### GoddessGawj

`hunger`

We Are Hungry!  We Must Feed!

### VO

`hunger`

Once again, the time of her hunger had awakened, the 3rd weekend of every month.

*sigh*

Just kidding!

_wink_

### GG 

Feed us...

SEVEN!

### VO

`confused`

Many were confused...

### 7SevenConfusedGawjerCompaltars

It applies to nothing

And to everything.

### VO

...which is...true

### 77CGC

So it's useless.

### VO

...which is...not so much...true.

### GG

And along the way

If you happen to pickup any of

Good Things Come To Those Who Wait

Player Standing Idle Helps Their Progression

Dare To Fail

Failure is Just Another Path to Victory

...or...and...

Roll a D20!

Include Some RPG Mechanics in Your Game (Stats, Skills, Etc)

(Perhaps D7 in this case?...D49?)

...then...I shall devour them as well!

*MWUHAHA*

### VO

SevenLovingGwajjers thought long and hard about what was said

### SevenLovingGwajjers

`i_love`
I love Lady Gwaj

`if_she_says`
If she says love Seven

`love_seven`
I will love seven

`chanting`

We are Seven, 7 of Us

### VO

`chanted_7` They chanted 7 times, 7 minutes, 7 days...

`even_now` Their chant can still be hear, even now

----------

### VO

The search for sevens continued

The swajjers et their Compaltars free upon the Internet

Seeking sevens to satisfy their cravings

And to feed their demanding lady

-----------

### VO

Seven Gwajjers found their way to mathland

The portals between anything

There this discovered 

The Path to Heptagons and Heptacles Through Points

---------

SevenGwajjers thought of games, and played with platformers and space shooters

----------

Seven Gwajjers remembered the Rainbow, seven colors, ROYGBIV, Red, Orange, Yellow, Green, Blue, Indigo, Violet

-----------

All were made offerings to the Great Gwaj

Fed into their Compaltars

And she grew, spreading her web

Everywhere

---------

SevenLoving were still chanting, and asked the 7 Questions:

Who...who will we love?  We will love Seven

What...what will we love?  7 and 7 times 7 again

Where...where will we love?  Through the Compaltar

When...now!

Why...For Goddess Gwaj!

How...by seeing 7 as us, and us as 7

How much...all that we have to give

----

### VO

But many balked at this crazy idea....

### BalkingGwajjers

How can you love 7?  It's just a number...

### VO

So the SevenLoving fed everything they had into their LovedCompaltar

To think about 7Sevens day and night long

Looking for answers to the Seven7Mysteries

And the most difficult seemed to me...How?

How to love 7?

--------


## Seven7(Paths of HaptHeartlessTinMen and LovingGawjerCompaltars)

