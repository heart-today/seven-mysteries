
# Architecture

## MVP

### Markdown Processing

mistune for Python is used to process all Markdown files into JSON files (*.md -> *.json)

`markdown_processor.py`

Currently, this means:

* README.md
* STORY.md
* DESIGN.md
* CONTENT.md
* MAP.md
* ARCHITECTURE.md

...just need one (1!) more for 7Seven!  PROCESS.md if necessary...?

### StoryPresenting

* JSON -> StoryModel
* StoryModel -> AVSpeechBubbles
  * Characters
  * VoiceOver
* StoryOrchestrator

## Components

* Hapt
  * Outline
    * Hapt1_6 - Haptagon
    * Hapt2_5 - Haptacle2
    * Hapt3_4 - Haptacle3
  * Filled with Qox
* Qox - QuadPix
  * Colorable
  * TBD? Outlinable (use Texture?)
* Pix
* Rainbow
  * ColorRange - array of Colors ie [0ROYGBIV1]
  * Interpolation/Sampling - cr.start(3).end(6).choose(2)
  * Continuous/Discrete
  * ROYGBIV + 1/0 default
  * ...but make flexible!
  * Processor, Resource or Node TBD?
* RainbowHapt

## Complete

## Polish

## Dreams

* RainbowHapt Z-changing for Polygon2Ds so 7HH7TM can enter as Platformer
* SevenMysteriesDiscordBot
