# From https://docs.godotengine.org/en/stable/getting_started/scripting/gdscript/gdscript_styleguide.html#code-order
# 01. tool
# 02. class_name
# 03. extends
extends CanvasLayer # Node2D
# 04. # docstring
# 
# 05. signals
signal _private() # warning if not wired, send only primitive data for network
signal public()
# 06. enums
# 07. constants
# 08. exported variables

# 09. public variables
var player_camera_id = -1 setget _set_player_camera_id
var events : SubscriptionList = null
# 10. private variables
# 11. onready variables
# UI Elements
onready var messaging_button := $HBoxContainer/MessagingButton
onready var messaging_container := $MessagingContainer
onready var red_graph_book_button := $RedGraphBookButton
onready var red_graph_book_container := $RedGraphBookContainer
# Components
onready var messaging := $MessagingContainer/NinePatchRect/Messaging
# 12. optional built-in virtual _init method
# 13. built-in virtual _ready method

# Called when the node enters the scene tree for the first time.
func _ready():
	#assert(player_camera_id != -1)
	assert(messaging_button)
	assert(messaging_container)
	assert(red_graph_book_button)
	assert(red_graph_book_container)

	assert(messaging)
	if Jm.manager():
		events = SubscriptionList.new(Jm.events(), self)
		events.listen("music", "_on_music")
	else:
		push_warning("No manager...running solo?")
		
# 14. remaining built-in virtual methods

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
   pass # Replace with function body.


func _get_configuration_warning() -> String:
	var big_warning = ""
	for warning in _get_configuration_warnings():
		big_warning += warning
	return big_warning

# 15. public methods

# Call whenever RAMAMAYANA expressess themselves...
func say(msg : String):
	messaging_button.text = msg
	messaging.say(msg)
	
# 16. private methods

func _set_player_camera_id(id):
	player_camera_id = id
	printt("set player camera id", id)


func _get_configuration_warnings() -> Array:
	 return _get_private_signal_warnings()

func _get_private_signals() -> Array:
	 return ["_private"] # Replace with your own signals, silly!

# Ensure self signals are connected to some node between parent and scene_root!
func _get_private_signal_warnings():
	var warnings = []
	for ps in _get_private_signals():
		var connection_list = get_signal_connection_list(ps)
		if connection_list.empty():
		   warnings.append("Connect %s signal between parent and scene_root\n" % ps)
	 return warnings


func _on_music(args):
	say("MUSIC %s #%d" % [args.type, args.which])



func _on_MessagingButton_pressed():
	messaging_button.visible = false
	messaging_container.visible = true
#	red_graph_book_button.visible = false

func _on_MessagingToSmall_pressed():
	messaging_button.visible = true
	messaging_container.visible = false
#	red_graph_book_button.visible = true

func _on_RedGraphBookButton_pressed():
	red_graph_book_button.visible = false
	red_graph_book_container.visible = true
#	messaging_button.visible = false

func _on_RedGraphBookToSmall_pressed():
	red_graph_book_button.visible = true
	red_graph_book_container.visible = false
#	messaging_button.visible = true


func _on_Messaging_said(msg):
	messaging_button.text = msg
