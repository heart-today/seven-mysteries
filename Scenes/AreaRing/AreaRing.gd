# From https://docs.godotengine.org/en/stable/getting_started/scripting/gdscript/gdscript_styleguide.html#code-order
# 01. tool
# 02. class_name
# 03. extends
extends Area2D
# 04. # docstring
# 
# 05. signals
signal _private() # warning if not wired, send only primitive data for network
signal public()
# 06. enums
# 07. constants
# 08. exported variables
# 09. public variables
var velocity = Vector2.ZERO * 10
# 10. private variables
# 11. onready variables
onready var ring := $Ring
# 12. optional built-in virtual _init method
# 13. built-in virtual _ready method

# Called when the node enters the scene tree for the first time.
func _ready():
	ring.collision_object = self
	pass # Replace with function body.


# 14. remaining built-in virtual methods

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
   pass # Replace with function body.


func _physics_process(delta):
	position += velocity * delta

func _get_configuration_warning() -> String:
	var big_warning = ""
	for warning in _get_configuration_warnings():
		big_warning += warning
	return big_warning

# 15. public methods
# 16. private methods
func _get_configuration_warnings() -> Array:
	 return _get_private_signal_warnings()

func _get_private_signals() -> Array:
	 return ["_private"] # Replace with your own signals, silly!

# Ensure self signals are connected to some node between parent and scene_root!
func _get_private_signal_warnings():
	var warnings = []
	for ps in _get_private_signals():
		var connection_list = get_signal_connection_list(ps)
		if connection_list.empty():
		   warnings.append("Connect %s signal between parent and scene_root\n" % ps)
	 return warnings




