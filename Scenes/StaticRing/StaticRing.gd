# From https://docs.godotengine.org/en/stable/getting_started/scripting/gdscript/gdscript_styleguide.html#code-order
# 01. tool
tool
# 02. class_name
# 03. extends
extends StaticBody2D
# 04. # docstring
# 
# 05. signals
# 06. enums
# 07. constants
# 08. exported variables
# 09. public variables
# Declare member variables here. Examples:
# var a = 2
# var b = "text"
# 10. private variables
# 11. onready variables
onready var ring = $Ring
# 
# 12. optional built-in virtual _init method
# 13. built-in virtual _ready method

# Called when the node enters the scene tree for the first time.
func _ready():
	ring.collision_object = self


# 14. remaining built-in virtual methods
# 15. public methods
# 16. private methods







