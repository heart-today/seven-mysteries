# From https://docs.godotengine.org/en/stable/getting_started/scripting/gdscript/gdscript_styleguide.html#code-order
# 01. tool
# 02. class_name
# 03. extends
extends Node2D
# 04. # docstring
# 
# 05. signals
signal _private() # warning if not wired, send only primitive data for network
signal public()
# 06. enums
# 07. constants

# 08. exported variables
export(PackedScene) var PLAYER_CAMERA_SCENE : PackedScene = preload("res://Scenes/PlayerCamera/PlayerCamera.tscn")
# 09. public variables
var events : SubscriptionList
#10. private variables
#11. onready variables
onready var sonnik := $"Sonnik 7-12"
onready var game_states := $GameStates

# 12. optional built-in virtual _init method
# 13. built-in virtual _ready method

# Called when the node enters the scene tree for the first time.
func _ready():
	events = SubscriptionList.new(Jm.events(), self)
	events.listen("preload_level", "_on_preload_level")
	events.listen("load_level", "_on_load_level")
	events.listen("level_finished", "_on_level_finished")
	events.listen("level_failed", "_on_level_failed")
	Jm.events().raise_event("my_event", 2)
	
	_on_load_level({ scene_id = Jm.manager().initial_scene_id })
	#game_states.transition_to("Level00")
	pass
	

func _on_preload_level(args):
	var level_id = args.scene_id
	var level_name = game_states.states[level_id].SCENE_FILE_NAME
	printt("preload level", level_id, level_name)
	
	Jm.manager().load_scene_async(level_name, level_id)
	
func _on_load_level(args):
	var level_id = args.scene_id
	var level_name = game_states.states[level_id].SCENE_FILE_NAME
	printt("load level", level_id, level_name)
	Jm.manager().load_scene(level_name, level_id)
	for i in Jm.manager().num_split_screen_viewports:
		var camera = PLAYER_CAMERA_SCENE.instance()
		camera.player_camera_id = i
		assert(camera.player_camera_id == i)
		Jm.manager().set_player_camera(camera, i, level_id)
		
		
func _on_level_finished(args):
	printt("level finished ", args)
	match args.scene_id:
		"Level00": 
			_on_load_level({ scene_id = "Level01"})
		"Level01": 
			_on_load_level({ scene_id = "Level02"})
		"Level02": 
			_on_load_level({ scene_id = "Level03"})
		"Level03": 
			_on_load_level({ scene_id = "Level04"})
		"Level04": 
			_on_load_level({ scene_id = "Level05"})
		"Level05": 
			_on_load_level({ scene_id = "Level06"})
		"Level06": 
			_on_load_level({ scene_id = "Level00"})
			
func _on_level_failed(args):
	printt("level failed ", args)
	_on_load_level(args)
	
# 14. remaining built-in virtual methods

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_just_pressed("mute"):
		var idx = AudioServer.get_bus_index("Music")
		AudioServer.set_bus_mute(idx, not AudioServer.is_bus_mute(idx))
		
	if Jm.manager().is_loading_scene():
		var progress = Jm.manager().get_load_scene_progress()
		# Display loading bar
	elif Jm.manager().failed_loading_scene():
		# Handle failure!
		assert(false)
	else:
		# SUCCESS!!!
		pass

func _get_configuration_warning() -> String:
	var big_warning = ""
	for warning in _get_configuration_warnings():
		big_warning += warning
	return big_warning

# 15. public methods
# 16. private methods
func _get_configuration_warnings() -> Array:
	 return _get_private_signal_warnings()

func _get_private_signals() -> Array:
	 return ["_private"] # Replace with your own signals, silly!

# Ensure self signals are connected to some node between parent and scene_root!
func _get_private_signal_warnings():
	var warnings = []
	for ps in _get_private_signals():
		var connection_list = get_signal_connection_list(ps)
		if connection_list.empty():
		   warnings.append("Connect %s signal between parent and scene_root\n" % ps)
	 return warnings






func _on_Sonnik_712_bass(which):
	Jm.events().raise_event("music", { type = "bass", which = which })


func _on_Sonnik_712_cello(which):
	Jm.events().raise_event("music", { type = "cello", which = which })


func _on_Sonnik_712_violin(which):
	Jm.events().raise_event("music", { type = "violin", which = which })


func _on_Sonnik_712_animation_finished(anim_name):
	sonnik.play("Play")


func _on_GameState_transitioned_to(state_name):
	printt("transitioned to", state_name)

