# From https://docs.godotengine.org/en/stable/getting_started/scripting/gdscript/gdscript_styleguide.html#code-order
#01. tool
tool
#02. class_name
#03. extends
extends KinematicBody2D
#04. # docstring
#
#05. signals
signal _propel(node, from, direction)

#06. enums
enum { NO_WALL, RUN_WALL, SLIDE_WALL, JUMP_WALL }
#07. constants
#08. exported variables
export var DISPLAY := true setget _set_display
export var enabled := true setget _set_enabled
export var jump_max := 2
export var jump_height := 500
export var gravity := 30
export var max_speed := 400
export var acceleration := 2000
export var friction := 1000
export var turret_scale := 0.1
export var propel_scale := 10.0
export var wall_slide_acceleration := 10
export var wall_slide_max_speed := 120
export var wall_jump_motion_scale := 2
export var head_index := 0 setget set_head_index, get_head_index
export var trunk_index := 0 setget set_trunk_index, get_trunk_index
#09. public variables
var actions setget _set_actions, _get_actions
#10. private variables
var _head_count := 17
var _trunk_count := 7
var _jumps_left := 0
var _jump_pressed := false
var _motion := Vector2.ZERO
var _wall_jump_motion_x := 0.0
#11. onready variables
onready var body := $Body
onready var head := $Body/Head
onready var trunk := $Trunk
onready var display := $Body/Display
onready var turret := $Trunk/Turret
onready var target := $Trunk/Turret/TargetingRing
onready var collision_shape := $FeetCollision
onready var wall_timer := $WallTimer
onready var point_cloud := $Body/PointCloud
#12. optional built-in virtual _init method
#13. built-in virtual _ready method
func _ready():
	print("gravity: %d" % gravity)
	print("editor? %s" % Engine.editor_hint)
	_set_enabled(enabled)
	
#14. remaining built-in virtual methods
var last_mouse_pos
func _process(delta):
	if turret:
#		var mp = get_global_mouse_position()
#		var mouse = turret.get_local_mouse_position()
#		turret.rotation += mouse.angle() * turret_scale
		
		turret.look_at(get_global_mouse_position())
	
func _physics_process(delta):
	if enabled and not Engine.editor_hint:
		_get_input(delta)
		_motion = move_and_slide(_motion, Vector2.UP)
		for i in get_slide_count():
			var collision : KinematicCollision2D = get_slide_collision(i)
			if not collision.collider is TileMap:
				print(collision.collider)
	
#15. public methods
func get_action_names():
	return actions.keys()
	
func do(action : String):
	return self.actions[action].do()
	
func set_head_index(i : int, wrap = true):
	if head:
		head.frame = i % _head_count if wrap else int(clamp(i, 0, _head_count - 1))

func get_head_index():
	return head.frame if head else -1
	
func set_trunk_index(i : int, wrap = true):
	if trunk:
		trunk.frame = i % _trunk_count if wrap else int(clamp(i, 0, _trunk_count - 1))

func get_trunk_index():
	return trunk.frame if trunk else -1
	
func change_trunk_index(delta : int, wrap = false):
	set_trunk_index(trunk_index + delta, wrap)
	
#16. private methods
func _set_display(d : bool):
	if display:
		display.visible = d
	elif not Engine.editor_hint:
		push_warning("display is null?")
	DISPLAY = d

func _set_enabled(d : bool):
	if collision_shape:
		collision_shape.set_deferred("disabled", not d)

	if body:
		body.visible = d
	elif not Engine.editor_hint:
		push_warning("body is null?")
	enabled = d
	
func _propel():
	var point = point_cloud.pop()
	if point:
		emit_signal("_propel", point, get_target_pos(), get_target_dir() * propel_scale)
	
func get_target_pos() -> Vector2:
	return target.global_position
	
func get_target_dir(as_degrees = false):
	return turret.global_rotation
	
var _d
var _c
func _display(d : String):
	if d == _d:
		_c += 1
	else:
		_d = d
		_c = 1
	if display:								# add aim point and direction
		display.text = ("%8s(%3d): f%d w%d c%d j%d %s a%s d%s" % 
		#display.text = ("%8s(%3d): f%d w%d c%d j%d %s" % 
			[d, _c, 
			1 if is_on_floor() else 0,
			1 if is_on_wall() else 0,
			1 if is_on_ceiling() else 0,
			_jumps_left, _motion,
			get_target_pos(),
			get_target_dir()
			])

# actions:
#   is_on_floor -> idling (contact FLOOR)
#   is_on_ceiling -> falling (contact CEILING)
#   is_on_wall -> walling (w/acceleration < gravity, contact WALL)
#   is_on_nothing -> falling (w/gravity, contact NOTHING)
#   jump -> rising (w/gravity)
#   move_left (move -1) -> moving_left (moving -1)
# 	move_right (move +1) -> moving_right (moving +1)
#	
# (TBD)
#	sevenify - turtle into just a 7! form
#   ringify - turn into a ring
#		heptacle2_5, heptacle3_4, heptagon(power of 1 or 2)
#   humanize - turn back into tinmen
#	duck
#	aim
#	shoot
#	
# states:
#	idling
#		is_on_floor -> assert(true)
#		is_on_ceiling -> assert(false)
#		is_on_wall	->	assert(false)
#	running
#		is_on_floor -> assert(true)
#		is_on_ceiling -> assert(false)
#	falling
#   		is_on_nothing -> assert(true)
#   rising
#		# Nothing different!
#	walling
# 		jump -> wall_jumping (moving AWAY_WALL)
#		move TOWARD_WALL -> walling
#		move AWAY_WALL	-> falling
#
func _get_input(delta):
	var do_jump := false
	var do_fall := false
	var do_wall := NO_WALL
	
	if Input.is_action_just_pressed("propel"):
		_propel()
		
	var input_x = Input.get_action_strength("ui_right")
	input_x -= Input.get_action_strength("ui_left")
	if _wall_jump_motion_x != 0:
		#_wall_jump_motion_x = move_toward(_wall_jump_motion_x, input_x * max_speed, acceleration * delta)
		_motion.x = _wall_jump_motion_x
	elif input_x == 0:
		_motion.x = move_toward(_motion.x, 0, friction * delta)
	else:
		_motion.x = move_toward(_motion.x, input_x * max_speed, acceleration * delta)

	if is_on_floor():
		_jumps_left = jump_max
		do_jump = _jump_pressed
		_motion.y = 0
	elif is_on_wall() and _motion.x != 0:
		do_wall = _wall_slide_or_jump(delta)
	else:
		#_coyote_time()
		do_fall = true
			
	if Input.is_action_just_pressed("ui_up"):
		# _remember_jump()
		do_jump = _jumps_left > 0 and do_wall != RUN_WALL
	
	if do_fall:
		_motion.y += gravity
		
	if do_jump or (do_wall == JUMP_WALL):
		_motion.y += -jump_height
		_jumps_left -= 1
		_display("jump")
	elif do_wall == JUMP_WALL:
		_motion.y += -jump_height
		_motion.x = -input_x
		_jumps_left -= 1
		_wall_jump_motion_x = _motion.x * -5 # -wall_jump_motion_scale
		_display("jump wall")
	elif do_wall == SLIDE_WALL:
		_motion.y = min(_motion.y + wall_slide_acceleration, wall_slide_max_speed)
		_display("slide wall")
	elif do_wall == RUN_WALL:
		_display("run wall")
	elif _motion.y > 0:
		_display("fall")
	elif _motion.y < 0:
		_display("rising")
	elif _motion.x == 0:
		_display("idle")
	else:
		#_display("run")
		pass
		

func _wall_slide_or_jump(_delta) -> int:
	var do_wall := SLIDE_WALL
	# _jumps_left = jump_max
	if is_on_floor():
		do_wall = RUN_WALL
	elif _motion.y > 0 and _jump_pressed:
		do_wall = JUMP_WALL
	return do_wall


func _coyote_time():
	yield(get_tree().create_timer(.1), "timeout")
	_jumps_left -= 1
	
	
func _remember_jump():
	_jump_pressed = true
	yield(get_tree().create_timer(.1), "timeout")
	_jump_pressed = false
		
func _set_actions(_actions):
	# SHOULD NEVER HAPPEN!
	assert(false)

class F:
	func _init(o, m, a := []): 
		_o = o; _m = m; _a = a
	func do(a := []): _o.callv(_m, _a + a)
	var _o; var _m; var _a

func a(m, a): return F.new(self, m, a)
			
# TODO Name all 17 smiles!
func _get_actions():
	# These actions are actually ViewActions
	# LOL!  These are abbreviations! Hahahaha!
	return {
		smile = a("set_head_index", [0]),
		grin = a("set_head_index", [1]),
		sad = a("set_head_index", [2]),
		frown = a("set_head_index", [3]),
		increment = a("change_trunk_index", [1, false]),
		decrement = a("change_trunk_index", [-1, false]),
	}



func _get_configuration_warning() -> String:
	var big_warning = ""
	for warning in _get_configuration_warnings():
		big_warning += warning
	return big_warning

# 15. public methods
	
# 16. private methods
func _get_configuration_warnings() -> Array:
	var warnings = _get_private_signal_warnings()
	if not display:
		warnings.append("Unable to find display @ $Body/Display\n")
	if not turret:
		warnings.append("Unable to find turret @ $Trunk/Turret\n")
	return warnings

func _get_private_signals() -> Array:
	 return ["_propel"] # Replace with your own signals, silly!

# Ensure self signals are connected to some node between parent and scene_root!
func _get_private_signal_warnings():
	var warnings = []
	for ps in _get_private_signals():
		var connection_list = get_signal_connection_list(ps)
		if connection_list.empty():
		   warnings.append("Connect %s signal between parent and scene_root\n" % ps)
	 return warnings
	

func _on_SwipeDetector_swipe_canceled(start_position):
	print("swipe_canceled: %s" % start_position)


func _on_SwipeDetector_swipe_detected(direction):
	var from = turret.position
	emit_signal("_propel", from, direction)
