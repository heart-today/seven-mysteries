# Based on https://www.youtube.com/watch?v=xsAyx2r1bQU
tool
extends RigidBody2D

enum WrapMode { WRAP, CLAMP, FOLLOW, MAX }

signal boarding_zone_entered(body)
signal boarded(boarder)
signal ejected(boarder, from, direction)

export var enabled = false setget _set_enabled
export (int) var engine_thrust = 16000
export (int) var spin_thrust = 16000
export (WrapMode) var wrap_mode = 0 setget _set_wrap_mode	# WRAP

var thrust := Vector2.ZERO
var rotation_dir := 0

onready var body_ring := $BodyRing
onready var cockpit := $Cockpit
onready var display := $Display
onready var thruster := $Thruster

func _ready():
	body_ring.collision_object = self


func _process(_delta):
	if enabled:
		_get_input()

class CanvasTransform:
	var canvas_item : CanvasItem
	func _init(ci : CanvasItem):
		canvas_item = ci
	func get_rect() -> Rect2:
		return Rect2(get_pos(),get_size())
	func get_scale() -> Vector2:
		return canvas_item.get_canvas_transform().get_scale()
	func get_center() -> Vector2:
		return get_pos() + 0.5*get_size()/get_scale()
	func get_pos() -> Vector2:
		var xform =  canvas_item.get_canvas_transform()
		return -xform.get_origin() / xform.get_scale()
	# https://www.reddit.com/r/godot/comments/aavjtn/get_viewport_size/
	func get_size() -> Vector2:
		return canvas_item.get_viewport_rect().size
	func wrap(pos : Vector2, mode ) -> Vector2:
		return Vector2()
	func clamp(pos : Vector2) -> Vector2:
		return Vector2()
	static func get_camera_center(ci : CanvasItem) -> Vector2:
		var xform = ci.get_canvas_transform()
		var top_left = -xform.get_origin() / xform.get_scale()
		var size = ci.get_viewport_rect().size
		return top_left + 0.5*size/xform.get_scale()
#	# https://godotengine.org/qa/4750/get-center-of-the-current-camera2d
#	func _get_camera_center():
#		var vtrans = get_canvas_transform()
#		var top_left = -vtrans.get_origin() / vtrans.get_scale()
#		var vsize = get_viewport_rect().size
#		return top_left + 0.5*vsize/vtrans.get_scale()
	
var _origin := Vector2.ZERO
func _integrate_forces(state):
	if enabled:
		set_applied_force(thrust.rotated(rotation))
		set_applied_torque(rotation_dir * spin_thrust)
		var xform = state.transform
		_origin = xform.origin
		var visible_rect = CanvasTransform.new(self).get_rect()
		if wrap_mode == WrapMode.WRAP:
			if xform.origin.x > visible_rect.end.x:
				xform.origin.x = visible_rect.position.x
			elif xform.origin.x < visible_rect.position.x:
				xform.origin.x = visible_rect.end.x
			if xform.origin.y > visible_rect.end.y:
				xform.origin.y = visible_rect.position.y
			elif xform.origin.y < visible_rect.position.y:
				xform.origin.y = visible_rect.end.y
		elif wrap_mode == WrapMode.CLAMP:
			xform.origin.x = clamp(xform.origin.x, visible_rect.position.x, visible_rect.end.x)
			xform.origin.x = clamp(xform.origin.y, visible_rect.position.y, visible_rect.end.y)
		state.transform = xform


func _get_configuration_warning() -> String:
	var big_warning = ""
	for warning in _get_configuration_warnings():
		big_warning += warning
	return big_warning

# 15. public methods
func board(boarder):
	boarder.enabled = false
	cockpit.add_child(boarder)
	boarder.global_position = cockpit.global_position
	self.enabled = true
	sleeping = false
	var remote_transform := boarder.get_node_or_null("RemoteTransform2D") as RemoteTransform2D
	if remote_transform:
		remote_transform.force_update_cache()
	emit_signal("boarded", boarder)
	

func eject():
	if cockpit and cockpit.get_child_count():
		var boarder = cockpit.get_child(0)
		
		var target_pos = boarder.get_target_pos()
		var target_dir = boarder.get_target_dir()

		remove_child(boarder)
		# TODO get target pos dir
		
		emit_signal("ejected", boarder, target_pos, target_dir)
	
		
# 16. private methods
var _d
var _c
func _display(d : String):
	if d == _d:
		_c += 1
	else:
		_d = d
		_c = 1
	if display:
		var visible_rect = CanvasTransform.new(self).get_rect()
		display.text = ("%s (%3d) t%s r%d w%d %.0f,%.0f %.0f,%.0fx%.0f,%.0f" % 
			[d, _c, thrust, rotation_dir, wrap_mode, 
			global_position.x, global_position.y, 
			visible_rect.position.x, visible_rect.position.y,
			visible_rect.end.x, visible_rect.end.y,
			])
			
func _get_input():
	if Input.is_action_just_pressed("wrap"):		
		wrap_mode = (wrap_mode + 1) % WrapMode.MAX
		print("wrap_mode: %d" % wrap_mode)
	
	if Input.is_action_just_pressed("eject"):	
		eject()
		
	var show_thrust
	if Input.is_action_pressed("ui_up"):
		thrust = Vector2(0, -engine_thrust)
		show_thrust = true
	else:
		thrust = Vector2.ZERO
		show_thrust = false
	
	if thruster:
		thruster.visible = show_thrust
	
	rotation_dir = 0
	if Input.is_action_pressed("ui_right"):
		rotation_dir += 1
	if Input.is_action_pressed("ui_left"):
		rotation_dir -= 1
	if thrust:
		_display("thrust")
	elif rotation_dir:
		_display("rotate")
	else:
		_display("idle")
		
func _set_wrap_mode(wm):
	wrap_mode = wm

func _get_configuration_warnings() -> Array:
	 return _get_private_signal_warnings()

func _get_private_signals() -> Array:
	 return ["boarding_zone_entered", "ejected"] # Replace with your own signals, silly!

# Ensure self signals are connected to some node between parent and scene_root!
func _get_private_signal_warnings():
	var warnings = []
	for ps in _get_private_signals():
		var connection_list = get_signal_connection_list(ps)
		if connection_list.empty():
		   warnings.append("Connect %s signal between parent and scene_root\n" % ps)
	 return warnings
	
func _set_enabled(e : bool):
	enabled = e
	
func _on_BoardingZone_body_entered(body):
	emit_signal("boarding_zone_entered", body)
		
		
		
