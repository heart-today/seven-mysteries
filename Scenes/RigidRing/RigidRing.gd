tool
extends RigidBody2D

onready var ring = $Ring

func _ready():
	if ring:
		ring.collision_object = self
