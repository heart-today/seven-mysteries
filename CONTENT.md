# CONTENT

[[_TOC_]]

## File Layout

* (Stories)
  * Markdown files at top level (res://*.md)
  * JSON files at top level (res://*.json)
  * mistune_story.py - Process *.md -> *.json
* Characters
  * Tin7Men (Player)
    * Body
      * Hand
      * Foot
      * Head
        * Face
      * Trunk
  * HaptHeart
  * Seven7ZER0s (Boss)
  * GoddessGwaj (Boss)
    * 7SevenGwajjers
    * Seven7Loving
  * CompAltar
    * ComputerHardware
  * Hapt
    * Qox
      * Pix
  * Point    
    * PointCloud
      * Polygon
      * Polygram
  * Colors
    * Black (0)
    * White (1)
    * Rainbow (ROYGBIV)
    * Numeral
    * Operator
      * OperatorPlus
      * OperatorMinus
      * OperatorMultiply
      * OperatorDivide
      * OperatorPoke
      * OperatorPeek
      * OperatorIterate
* Acts
  * Lilas (Sanskrit: “play,” “sport,” “spontaneity,” or “drama”) in Hinduism,
    * REMEMBER LilActs
* Assets
* Components
  * StoryOrchestrator
  * SwipeDetector
  * AudioOrchestrator
  * StateMachinery (Load/Save)
  * Player
  * Character
  * Boss
* Intro
* StatsLevel
* Tutorial
  * LevelPointOne
  * LevelPointTwo
  * LevelPointThree
  * LevelPointFour
  * LevelPointFive
  * LevelPointSix
  * LevelPointSeven
* Level
  * LevelOne
  * LevelTwo
  * LevelThree
  * LevelFour
  * LevelFive
  * LevelSix
  * LevelSeven
  * LevelZero
* AlternateLevel (012345676543210)
  * LevelSeven-6
  * LevelSeven-5
  * LevelSeven-4
  * LevelSeven-3
  * LevelSeven-2
  * LevelSeven-1
  * LevelSeven
  * LevelSeven+1
  * LevelSeven+2
  * LevelSeven+3
  * LevelSeven+4
  * LevelSeven+5
  * LevelSeven+6
  * LevelZero
* Map
* Ring
* UI
  * MainWindow (Start?)
  * SettingsWindow

## Characters

### Numeral

#### States

#### Actions

## Attributions

### Tutorials

* [Coyote Time Jumping](https://youtu.be/LeaaKRufdMc - Thanks, Redlive#9142

### Content

* Fancy1234567.png - ImperialBrake78#3080
* hardware_transparent_bg.png - [Hobo](https://opengameart.org/content/old-computer-hardware)

* computer-noise.mp3 - [aquinn](https://opengameart.org/content/retro-computer-noise)
* Arturia ARP2600V - 1950 Computer - [50 Sounds]_MP3_320kbps_by_jalastram.zip - [jalastram](https://opengameart.org/content/1950-computer-50-sounds)
* Typing Sfx.wav - [Nicole Marie T](https://opengameart.org/content/typing-on-computer-keyboard-sfx-sound-effect)

* Xfcn-SmilinJoe.7z - [Xfcn](https://opengameart.org/content/xfcn%E2%80%99s-smilin%E2%80%99-joe)

* techblocks2.png - [bart](https://opengameart.org/content/sci-fi-platformer-tiles-32x32)

* rpgicons.png - [Joe Williamson](https://opengameart.org/content/roguelikerpg-icons)
