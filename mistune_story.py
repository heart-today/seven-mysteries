#! /usr/bin/env python3

import mistune
import sys
import json

from os.path import basename, splitext


markdown = mistune.create_markdown(renderer=mistune.AstRenderer())
# markdown = mistune.create_markdown()

# YourGameCharacter

for file_name in sys.argv[1:]:
    n,x = splitext(basename(file_name))
    assert(x == '.md')

    j = n + '.json'
    with open(file_name, 'r') as i:
        data = markdown(i.read())

        with open(j, 'w') as o:
            o.write(json.dumps(data, indent = 4))
