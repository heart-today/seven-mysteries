extends Sprite

var target:Vector2

var speech: String setget _set_speech
onready var label = $PanelContainer/HBoxContainer/Label

export (Array, String) var greetings = [
	"7", "Seven", "Rainbows are ROYGBIV...7!", "Why are Seven7s disappearing?", 
	"Numeral!", "Have you heard of ZEROES?", "Should 7 hit Escape to move on?",
	"THE TIMER...IT IS 49 SECONDS!", "GoddessGwaj wants ALL OF US!", "The Gwajjers Are Coming!",
	"7letters7", "One Rainbow color at a time...", "M4MUTE? 7AGAIN!", "R RESET ?"]

func _set_speech(val):
	if val == "":
		$PanelContainer.hide()
	else:
		$PanelContainer.show()	
		label.text = str(val)
	speech = val
