# From https://docs.godotengine.org/en/stable/getting_started/scripting/gdscript/gdscript_styleguide.html#code-order
# 01. tool
# 02. class_name
# 03. extends
extends Node2D
# 04. # docstring
# 
# 05. signals
signal _private() # warning if not wired, send only primitive data for network
signal public()
# 06. enums
# 07. constants
# 08. exported variables
# 09. public variables
# Declare member variables here. Examples:
# var a = 2
# var b = "text"
# 10. private variables
# 11. onready variables
onready var tin7men = $Tin7Men
onready var ring_ship = $RingShip
# 12. optional built-in virtual _init method
# 13. built-in virtual _ready method

# Called when the node enters the scene tree for the first time.
func _ready():
	tin7men.enabled = true
	
# 14. remaining built-in virtual methods

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
   pass # Replace with function body.


func _get_configuration_warning() -> String:
	var big_warning = ""
	for warning in _get_configuration_warnings():
		big_warning += warning
	return big_warning

# 15. public methods
# 16. private methods
func _get_configuration_warnings() -> Array:
	 return _get_private_signal_warnings()

func _get_private_signals() -> Array:
	 return ["_private"] # Replace with your own signals, silly!

# Ensure self signals are connected to some node between parent and scene_root!
func _get_private_signal_warnings():
	var warnings := Array()
	for ps in _get_private_signals():
		var connection_list = get_signal_connection_list(ps)
		if connection_list.empty():
		   warnings.append("Connect %s signal between parent and scene_root\n" % ps)
	 return warnings






func _on_RingShip_body_entered(body):
	print("body entered RingShip: %s" % body)


func _on_RingShip_body_exited(body):
	print("body exited RingShip: %s" % body)


func _on_RingShip_boarding_zone_entered(body):
	if body.name == "Tin7Men":
		remove_child(tin7men)
		ring_ship.call_deferred("board", tin7men)


func _on_FellThrough_body_entered(body):
	printt("OOOOOHHHH NOOOOO!!!  You fell through...you are still falling...wow")
	Jm.events().raise_event("level_failed", { scene_id = "Level01" })

func _on_Exit_body_entered(body):
	if not body is TileMap:
		Jm.events().raise_event("level_finished", { scene_id = "Level01" })
