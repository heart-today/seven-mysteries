# From https://docs.godotengine.org/en/stable/getting_started/scripting/gdscript/gdscript_styleguide.html#code-order
# 01. tool
# 02. class_name
# 03. extends
extends "res://addons/jm_game_template/jm_state_machine.gd"
# 04. # docstring
# 
# 05. signals
signal _private() # warning if not wired, send only primitive data for network
signal public()
# 06. enums
# 07. constants
# 08. exported variables
# 09. public variables
# Declare member variables here. Examples:
# var a = 2
# var b = "text"
# 10. private variables
# 11. onready variables
# 
# 12. optional built-in virtual _init method
# 13. built-in virtual _ready method

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# 14. remaining built-in virtual methods

func _unhandled_key_input(event : InputEventKey):
	if event.scancode == KEY_ESCAPE and event.pressed == false:
		if Jm.manager():
			Jm.events().raise_event("level_finished", { scene_id = "Level00" })
		else:
			push_warning("manager not available...running without manager?")
	#if event
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
   pass # Replace with function body.


func _get_configuration_warning() -> String:
	var big_warning = ""
	for warning in _get_configuration_warnings():
		big_warning += warning
	return big_warning

# 15. public methods
# 16. private methods
func _get_configuration_warnings() -> Array:
	 return _get_private_signal_warnings()

func _get_private_signals() -> Array:
	 return ["_private"] # Replace with your own signals, silly!

# Ensure self signals are connected to some node between parent and scene_root!
func _get_private_signal_warnings():
	var warnings = []
	for ps in _get_private_signals():
		var connection_list = get_signal_connection_list(ps)
		if connection_list.empty():
		   warnings.append("Connect %s signal between parent and scene_root\n" % ps)
	 return warnings






func _on_Area2D_input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton:
		if Jm.manager():
			Jm.events().raise_event("level_finished", { scene_id = name })
		else:
			push_warning("manager not available...running without manager?")
		printt("input event", viewport, event, shape_idx)

func _on_Area2D_mouse_entered():
	pass # Replace with function body.


func _on_Area2D_mouse_exited():
	pass # Replace with function body.


func _on_Timer_timeout():
	print(name)
	#ssert(name == "Level00")
	if Jm.manager():
		Jm.events().raise_event("level_finished", { scene_id = "Level00" })
	else:
		push_warning("No manager...running solo?")
