# From https://docs.godotengine.org/en/stable/getting_started/scripting/gdscript/gdscript_styleguide.html#code-order
# 01. tool
# 02. class_name
# 03. extends
extends Node2D
# 04. # docstring
# 
# 05. signals
signal _private() # warning if not wired, send only primitive data for network
signal public()
# 06. enums
# 07. constants
# 08. exported variables
# 09. public variables
# Declare member variables here. Examples:
# var a = 2
# var b = "text"
# 10. private variables
# 11. onready variables
onready var tin7men = $Tin7Men
onready var ring_ship = $RingShip
# 12. optional built-in virtual _init method
# 13. built-in virtual _ready method

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# 14. remaining built-in virtual methods

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
   pass # Replace with function body.


func _get_configuration_warning() -> String:
	var big_warning = ""
	for warning in _get_configuration_warnings():
		big_warning += warning
	return big_warning

# 15. public methods
# 16. private methods
func _get_configuration_warnings() -> Array:
	 return _get_private_signal_warnings()

func _get_private_signals() -> Array:
	 return ["_private"] # Replace with your own signals, silly!

# Ensure self signals are connected to some node between parent and scene_root!
func _get_private_signal_warnings():
	var warnings = []
	for ps in _get_private_signals():
		var connection_list = get_signal_connection_list(ps)
		if connection_list.empty():
		   warnings.append("Connect %s signal between parent and scene_root\n" % ps)
	 return warnings






func _on_RingShip_boarding_zone_entered(body):
	if body == tin7men and body.get_parent() == self:
		var remote_transform = tin7men.get_node("RemoteTransform2D")
		tin7men.remove_child(remote_transform)
		remove_child(tin7men)
		ring_ship.board(tin7men)
		ring_ship.gravity_scale = 1.0
		ring_ship.add_child(remote_transform)
#		#ring_ship.wrap_mode = ring_ship.WrapMode.WRAP


func _on_Gateway_body_entered(body):
	if body == tin7men or body == ring_ship:
		printt("level finished", body)
		if Jm.manager():
			Jm.events().raise_event("level_finished", { scene_id = "Level05" })
		else:
			push_warning("No manager...why you doing that? ;-)")
