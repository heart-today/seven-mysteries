# Attribution - Tutorial https://www.youtube.com/watch?v=7XlMqjikI9A
# From https://docs.godotengine.org/en/stable/getting_started/scripting/gdscript/gdscript_styleguide.html#code-order
# 01. tool
tool
# 02. class_name
# 03. extends
extends Node
# 04. # docstring
# 
# 05. signals
signal swipe_detected(direction)

signal _swipe_started(position)
signal _swipe_merged(merged_start_position)
signal _swipe_timeout(start_position)
signal _swipe_canceled(start_position)
# 06. enums
enum { SWIPE_STARTED, SWIPE_MERGED, SWIPE_TIMEOUT, SWIPE_TOO_DIAGONAL, SWIPE_DETECTED }
# 07. constants
# 08. exported variables
export(float, 1.0, 1.5) var MAX_DIAGONAL_SLOPE = 1.3
export(bool) var MERGE_MULTIPLE_START_SWIPES = true # Otherwise make lots of little swipes!
# 09. public variables
# Declare member variables here. Examples:
# var a = 2
# var b = "text"
# 10. private variables
var _start_position = null
# 11. onready variables
onready var timer := $Timer
#
# 12. optional built-in virtual _init method
# 13. built-in virtual _ready method
# 14. remaining built-in virtual methods
func _input(event):
	if event is InputEventPanGesture:
		emit_signal("swipe_detected", event.delta)
		
	if event is InputEventScreenTouch:
		if event.pressed:
			_start(event.position)
		elif not timer.is_stopped():
			_end(event.position)


# Ensure self signals are connected to some node between parent and scene_root!
func _get_configuration_warning() -> String:
	var warning = ""
	for swipe in ["swipe_detected"]:
		var connection_list = get_signal_connection_list(swipe)
		if connection_list.empty():
			warning += "Connect %s signal between parent and scene_root\n" % swipe
	
	return warning
	
# 15. public methods
# 16. private methods
func _start(position : Vector2):
	assert(position)
	
	if _start_position and MERGE_MULTIPLE_START_SWIPES:
		emit_signal("swipe_merged", position)
	else:
		_start_position = position
		timer.start()
		emit_signal("swipe_started", position)
	
func _end(position : Vector2):
	timer.stop()
	var direction = (position - _start_position).normalized()
	var adx = abs(direction.x)
	var ady = abs(direction.y)

	var signal_name : String
	var signal_vector : Vector2
	
	if adx >= MAX_DIAGONAL_SLOPE:
		signal_name = "swipe_canceled"
		signal_vector = _start_position
	# -sign because swipe left moves screen right, and vice versa
	# Horizontal swipe
	elif adx > ady:
		signal_name = "swipe_detected"
		signal_vector = Vector2(-sign(direction.x), 0.0)
	else:
		# Vertical swipe
		signal_name = "swipe_detected"
		signal_vector = Vector2(0.0, -sign(direction.y))
		
	emit_signal(signal_name, signal_vector)
	_start_position = null

func _on_Timer_timeout():
	emit_signal("swipe_timeout", _start_position)
