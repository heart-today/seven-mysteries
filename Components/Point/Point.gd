tool
extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export var radius = 5 setget _set_radius
export(Color) var color = Color.black setget _set_color

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _draw():
	draw_circle(Vector2.ZERO, radius, color)
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _set_radius(r):
	radius = r
	update()
	
func _set_color(c):
	color = c
	update()
