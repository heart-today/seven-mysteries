extends Node2D

export var follow_scale = 0.1

onready var orbit = $Orbit

func push(node : Node):
	var success = false
	var follow = find_empty()
	if follow:
		follow.add_child(node)
		success = true
	return success

func pop():
	var child = null
	var follow = find_full()
	if follow:
		child = follow.get_child(0)
		follow.remove_child(child)
	return child
	
func find_empty():
	var empty = null
	for follow in orbit.get_children():
		if follow.get_child_count() == 0:
			empty = follow
			break
	return empty

func find_full():
	var full = null
	for follow in orbit.get_children():
		if follow.get_child_count() != 0:
			full = follow
			break
	return full
	
func _ready():
	var i = 0
	var delta = 1.0 / orbit.get_child_count()
	for follow in orbit.get_children():
		follow.unit_offset = i * delta
		i += 1

func _process(delta):
	for follow in orbit.get_children():
		follow.unit_offset += follow_scale * delta
