# Based on https://www.reddit.com/r/godot/comments/bux2hs/how_to_use_godots_high_level_multiplayer_api_with/
extends Node

signal received(id, cmd)

const PORT = 21000
const ADDR = "seven-mysteries.com"
const CONNECTION_RETRY_WAIT_SECONDS = 5

# Called when the node enters the scene tree for the first time.
func _ready():
	_create_peer()
		
	var tree := get_tree()
	
	var error = tree.connect("connected_to_server", self, "_on_connected_to_server")
	assert(OK == error)
	
	error = tree.connect("connection_failed", self, "_on_connection_failed")
	assert(OK == error)
	
	error = tree.connect("network_peer_connected", self, "_on_network_peer_connected")
	assert(OK == error)
	
	error = tree.connect("network_peer_disconnected", self, "_on_network_peer_disconnected")
	assert(OK == error)
	
	error = tree.connect("server_disconnected", self, "_on_server_disconnected")
	assert(OK == error)


func send(cmd : Dictionary):
	rpc_id(1, "_send", cmd)
	

func _valid_command_id(_cmd, _id):
	return true
	
master func _send(cmd):
	var sender_id = get_tree().get_rpc_sender_id()
	
	if _valid_command_id(cmd, sender_id):
		rpc("_receive", sender_id, cmd)

puppetsync func _receive(id, cmd):
	emit_signal("received", id, cmd)

func notify(msg : String, id = null):
	send({ msg = msg, id = id })
	
# Client
func _on_connected_to_server():
	notify("_on_connected_to_server")
	
# Client
func _on_connection_failed():	
	notify("_on_connection_failed")
	yield(get_tree().create_timer(CONNECTION_RETRY_WAIT_SECONDS), "timeout")
	_create_client()

	
func _on_network_peer_connected(id : int):
	notify("_on_network_peer_connected", id)
	
func _on_network_peer_disconnected(id : int):
	notify("_on_network_peer_disconnected", id)

# Client
func _on_server_disconnected():
	notify("_on_server_disconnected")
	yield(get_tree().create_timer(CONNECTION_RETRY_WAIT_SECONDS), "timeout")
	_create_client()
	

func _create_peer():
	if OS.has_feature("HTML5"):
		_create_client()
	else:
		if OK != _create_server():
			_create_client()

func _create_server():
	var server := WebSocketServer.new()

	var error = server.listen(PORT, PoolStringArray(), true)
	if OK == error:
		get_tree().set_network_peer(server)
	return error
	
func _create_client():
	var client := WebSocketClient.new()

	# You use "ws://" at the beginning of the address for WebSocket connections
	var url = "ws://" + str(ADDR) + ":" + str(PORT) 

	var error = client.connect_to_url(url, PoolStringArray(), true)
	if OK == error:	
		get_tree().set_network_peer(client)
	else:
		yield(get_tree().create_timer(CONNECTION_RETRY_WAIT_SECONDS), "timeout")
		call_deferred("_create_client")
	
func _process(_delta):
	var peer = get_tree().network_peer
	if peer and peer.get_connection_status() != NetworkedMultiplayerPeer.CONNECTION_DISCONNECTED:
		peer.poll()
