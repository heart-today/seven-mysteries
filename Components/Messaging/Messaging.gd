extends VBoxContainer

signal said(msg)

onready var lineEdit := $LineEdit
onready var label := $RichTextLabel
onready var ping := $AudioStreamPlayer

var network = null setget _set_network

func say(msg):
	label.add_text(msg)
	label.add_text("\n")
	emit_signal("said", msg)
	
func _ready():
	if not network:
		self.network = Network
		assert(network)

func _set_network(_network):
	if network:
		network.disconnect("received", self, "_on_Network_received")
	network = _network
	if network:
		var error = network.connect("received", self, "_on_Network_received")
		assert(OK == error)
	
func _on_Network_received(id, cmd):
	var text = "NETWORK %d: %s" % [id, cmd]
	ping.play()
	say(text)

func _on_LineEdit_text_entered(new_text):
	network.send({ msg = new_text })
	lineEdit.clear()
