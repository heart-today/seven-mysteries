# From https://docs.godotengine.org/en/stable/getting_started/scripting/gdscript/gdscript_styleguide.html#code-order
#01. tool
tool
#02. class_name
#03. extends
extends Node
#04. # docstring
"""
Loads Story JSON files (and other document JSON files at the moment)
Singleton autoloaded currently as Story
"""
# TODO
#05. signals
# Actions - Model Notifications/Emitters
# 
# Merged between Game Applications over the Network
# logos = { ProjectSettings/General/Application/Config/Name = { StoryName:  story lila 
signal story_started(story_name)
signal act_started(story_name,scene_name)
# full_names = { ShortName: FullName }
# short_names = { FullName: ShortNames
# 
# abbreviations = { abbreviation_name = ShortName, full_names: [{ FullName: [short_names] }], short_names: { ShortName: [full_names] } }
# emotions = { EmotionName: { lines = [line] }
#
# stories 
# story
# characters
# character
# lines = 
# logos = { name = story_name, lila_name, character_name, line_label, abbreviation_names: []
# lila = { name = lila_name, ... }  Think...Lil'A'ct...Lilas Are Acts Within Stories
# lilas = { LilaName: { lila } }
# scenes = 
# scene
# character = { logos_name, story_name, character_name, lila_names: [], abbreviations: [], emotion_names: []}
# line = { logos_name, story_name, lila_name, character_name, line_label, abbreviation_name, text, emotion_name )
signal line_emoted(story_name, scene_name, character_name, line) 
signal act_ended(story_name, scene_name)
signal story_ended()

signal abbreviations_requested(abbreviations)
signal emotions_requested(emotions)

signal stories_requested()
signal story_request_started(story_name)
signal story_requested(story)

# Views on the Model
signal _file_found(file_name)
signal _parse_erred(error)
#06. enums
#07. constants
const NAME = "name"
const SCENE = "Scene"


const ABBREVIATIONS = "abbreviations"

const LINES = "lines"
const LINE = "line"
const LABEL = "label"
const AUDIO = "audio"
enum { INVALID_LEVEL, STORY_LEVEL, SCENE_LEVEL, CHARACTER_LEVEL }
#08. exported variables
#09. public variables
var raw_stories = {}		# { file_name: raw_story }
var raw_documents = {} 	# { file_name: raw_document }
# { name: StoryName, 
#   abbreviations: {},
#   scenes: [ { name: SceneName, 
#               abbreviations: {}, 
#               characters: [ { CharacterNameOrAbbreviation: [ { label: Label, 
#                                              line: Line, 
#                                              audio: AudioAbbreviationsStorySceneCharacterLabelOggWavFileName } ] } ] 
#           } ] }
var stories = {} 
#10. private variables
var _started_scene = null
var _character_index = 0
var _line_index = 0
#11. onready variables
#
#12. optional built-in virtual _init method
#13. built-in virtual _ready method
func _ready():
	_parse_stories()

const HEADING = "heading"
const PARAGRAPH = "paragraph"
const BLOCK_CODE = "block_code"
const CODESPAN = "codespan"
const EMPHASIS = "emphasis"
const TEXT = "text"

class AST:
	extends Node
	func _init(t : String):
		type = t
	var type := ""

class HeadingAST:
	extends AST
	
	
class TextAST:
	extends AST
	func _init(tx : String).(TEXT):
		text = tx
	var text := ""


#14. remaining built-in virtual methods

# Ensure self signals are connected to some node between parent and scene_root!
func _get_configuration_warning() -> String:
	var warning = ""
	for swipe in ["swipe_detected", "swipe_canceled"]:
		var connection_list = get_signal_connection_list(swipe)
		if connection_list.empty():
			warning += "Connect %s signal between parent and scene_root\n" % swipe
	
	return warning
	
#15. public methods			

func get_story_name():
	var name = null
	if len(stories) == 1:
		name = stories.keys()[0]
	return name

func get_scene_names(story_name = null):
	if story_name == null:
		story_name = get_story_name()
	assert(story_name)
	assert(story_name in stories)
	return stories[story_name].scenes.keys()


func start_scene(scene_name : String, story_name = null) -> bool:
	if _started_scene:
		return false
	
	if story_name == null:
		story_name = get_story_name()
	var scenes = stories[story_name].scenes
	assert(scene_name in scenes)
	_started_scene = scenes[scene_name]
	
	emit_signal("scene_started", scene_name)
	next_line()
	
	return true


func next_line():
	if not _started_scene:
		return
	
	var characters = _started_scene.characters
	var character_count = len(characters)
	
	if _character_index == character_count:
		end_scene()
	else:
		var character = characters[_character_index]
		var character_name = character.keys()[0]
		var lines = character[character_name]
		var line_count = len(lines)
		if _line_index < line_count:
			if character_name in _started_scene[ABBREVIATIONS]:
				character_name = _started_scene[ABBREVIATIONS][character_name]
			emit_signal("line_spoken", character_name, lines[_line_index])
			_line_index += 1
		if _line_index == line_count:
			_line_index = 0
			_character_index += 1

		
func end_scene():
	if not _started_scene:
		return
		
	emit_signal("scene_ended")
	_started_scene = null
	_character_index = 0
	_line_index = 0
		
# Thanks to https://godotengine.org/qa/5175/how-to-get-all-the-files-inside-a-folder
func list_files_in_directory(path):
	var files = []
	var dir = Directory.new()
	dir.open(path)
	dir.list_dir_begin()

	while true:
		var file = dir.get_next()
		if file == "":
			break
		elif not file.begins_with("."):
			var full_file = path + file
			emit_signal("_file_found", full_file)
			files.append(full_file)

	dir.list_dir_end()

	return files



func _parse_stories():
	var files = list_files_in_directory("res://")
	for f in files:
		var file_name : String = f as String
		if file_name.ends_with("STORY.json"):
			assert(not file_name in raw_stories)
			var raw_story = load_json(file_name)
			assert(raw_story)
			
			raw_stories[file_name] = raw_story
			
			# Writes directly into stories!
			_process_raw_story(raw_story)
			
		elif file_name.ends_with(".json"):
			assert(not file_name in raw_documents)
			var raw_document = load_json(file_name)
			assert(raw_document)
			
			raw_documents[file_name] = raw_document
			
func _parse_json(text, file_name):
	var result
	var json_result := JSON.parse(text)

	if json_result.error == OK:
		# File contains valid JSON
		result = json_result.result
	else:
		# File contains invalid JSON
		_error(
				"Error while parsing JSON at {file_name}:{line}: {message}".format({
						file_name = file_name,
						line = json_result.error_line,
						message = json_result.error_string,
				})
		)
	return result

func load_json(file_name):
# Will contain the JSON parsing result if valid (typically a dictionary or array)
	var result

	var file = File.new()
	var file_error = file.open(file_name, File.READ)

	if file_error == OK:
			result = _parse_json(file.get_as_text(), file_name)
	else:
		# An error occurred related to the file reading (e.g. the file wasn't found)
		_error("Could not open file: %s" % file_name)
		
	return result
	

#16. private methods

# 16.B utility methods

func _error(error):
	push_error(error)
	emit_signal("error", error)
	
# PROBLEM  This layout kind of assumes that scenes are linear...true in a script,
# not so in a video game.
#
# { name: StoryName, 
#   abbreviations: {},
#   scenes: [ { name: SceneName, 
#               abbreviations: {}, 
#               characters: [ { CharacterName: [ { label: Label, 
#                                              line: Line, 
#                                              audio: AudioAbbreviationsStorySceneCharacterLabelOggWavFileName } ] } ] 
#           } ] }
func _process_raw_story(raw_story):
	var story = null
	var scenes = null
	var scene = null
	var characters = null
	var character = null
	var lines = null
	var label = null
	var heading_level = INVALID_LEVEL
		
	for entry in raw_story:
		match entry.type:
			HEADING:
				heading_level = int(entry.level)
				match heading_level:
					STORY_LEVEL:
						assert(not story)
						scenes = {}
						story = { 
							name = entry.children[0].text,
							scenes = scenes,
							ABBREVIATIONS: {},
						}
						# print("story %s" % story)
						assert(not scenes)
						scene = null
						characters = null
						character = null
						lines = null
						label = null
					SCENE_LEVEL:
						assert(story)
						characters = []
						scene = { 
							name = entry.children[0].text,
							characters = characters,
							ABBREVIATIONS: {}
						 }
						# print("scene %s" % scene)
						if scene.name != SCENE:
							assert(not scene.name in scenes)
							scenes[scene.name] = scene
						# Clear everything out
						character = null
						lines = null
						label = null
					CHARACTER_LEVEL: 
						assert(scene)
						lines = []
						# TODO: Make this like STORY/SCENE above, not singleton dict
						character = { entry.children[0].text: lines }
						# print("character %s" % character)
						characters.append(character)
						label = null
					_:
						#print("type %d" % typeof(heading_level))
						#print("Unexpected heading level %d" % heading_level)
						assert(false)
			BLOCK_CODE:
				var data = parse_json(entry.text)
				assert(data)
				if ABBREVIATIONS in data:
					var abbr = data[ABBREVIATIONS]
					#print("abbreviations %s" % abbr)
					if scene:
						scene[ABBREVIATIONS] = abbr
					else:
						story[ABBREVIATIONS] = abbr
			PARAGRAPH:
				for child in entry.children:
					match child.type:
						CODESPAN:
							label = child.text
							#print("label %s" % label)
						EMPHASIS:
							for c in child.children:
								label = _handle_line(story, scene, character, lines, label, c.text, true)
						TEXT:
							label = _handle_line(story, scene, character, lines, label, child.text)


	assert(story.name)
	assert(not story.name in stories)
	stories[story.name] = story
	
	#print(JSON.print(story, "\t"))
	print(get_scene_names())
	
# TODO NYI !!!
# Create label via snake_case if not there 
# Create audio via story/scene/character/label and em
# Verify audio files exist
func _handle_line(story, scene, character, lines, label, text, em = false):
	var line = {
		line = text,
		# The 'em' stands for both EMPHASIS and EMOTE!
		em = em
	}
	if label:
		line.label = label
		# Clear out now used label
		label = null
	#print("line %s" % line)
	
	# TODO Why do I need to check lines?
	if lines != null:
		lines.append(line)
	
	return label
