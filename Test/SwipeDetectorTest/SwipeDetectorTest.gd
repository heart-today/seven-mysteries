# From https://docs.godotengine.org/en/stable/getting_started/scripting/gdscript/gdscript_styleguide.html#code-order
# 01. tool
# 02. class_name
# 03. extends
extends Node
# 04. # docstring
# 
# 05. signals
# 06. enums
# 07. constants
# 08. exported variables
# 09. public variables
# Declare member variables here. Examples:
# var a = 2
# var b = "text"
# 10. private variables
# 11. onready variables
onready var swipe_detector = $SwipeDetector
# 
# 12. optional built-in virtual _init method
# 13. built-in virtual _ready method
# 14. remaining built-in virtual methods
# 15. public methods
# 16. private methods

# TODO Move this deduplication back into SwipeDetector!
var previous : Vector2
func _on_SwipeDetector_swipe_detected(direction : Vector2):
	var dn = direction.normalized()
	if previous and previous.is_equal_approx(dn):
		print("swipe duplicate ignored: %s" % previous)
	elif dn.is_equal_approx(Vector2.ZERO):
		print("swipe zero ignored")
	else:
		previous = dn
		print("swipe_detected direction: %s" % dn)
