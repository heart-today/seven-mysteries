extends Control

onready var start_scene := $VBoxContainer/StartScene
onready var label := $VBoxContainer/Label
onready var next_line := $VBoxContainer/NextLine
onready var cancel_scene := $VBoxContainer/CancelScene

# Called when the node enters the scene tree for the first time.
func _ready():
	Story.connect("scene_started", self, "_on_Story_scene_started")
	Story.connect("line_spoken", self, "_on_Story_line_spoken")
	Story.connect("scene_ended", self, "_on_Story_scene_ended")	
	Story.connect("parse_erred", self, "_on_Story_parse_erred")
	Story.connect("file_found", self, "_on_Story_file_found")

func line(text):
	label.append_bbcode("[color=#000000]%s[/color]" % text)

func _on_Story_scene_started(scene_name):
	start_scene.visible = false
	label.visible = true
	label.clear()
	next_line.visible = true
	cancel_scene.visible = true
	line("Scene: %s\n" % scene_name)

func _on_Story_line_spoken(character_name, line):
	var text = line.line
	if line.em:
		text = "*%s*" % text
	line("%s: %s\n" % [character_name, text])
	
func _on_Story_scene_ended():
	line("THE END")
	next_line.visible = false
	cancel_scene.visible = false
	#yield(get_tree().create_timer(2), "timeout")
	label.visible = false
	start_scene.visible = true

func _on_Story_parse_erred(error):
	line("ERROR: %s" % error)

func _on_Story_file_found(file):
	line("file: %s" % file)
		
func _on_NextLine_pressed():
	Story.next_line() # Replace with function body.

func _on_StartScene_pressed():
	var scene_names = Story.get_scene_names()
	line("scene names: %s" % str(scene_names))
	Story.start_scene(scene_names[0])


func _on_CancelScene_pressed():
	Story.end_scene()
