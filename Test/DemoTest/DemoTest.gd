# From https://docs.godotengine.org/en/stable/getting_started/scripting/gdscript/gdscript_styleguide.html#code-order
#01. tool
#02. class_name
#03. extends
extends Control
#04. # docstring
#05. signals
#06. enums
#07. constants
#08. exported variables
export var follow_scale = 0.05
export var code_link = "https://gitlab.com/heart-today/seven-mysteries"
#09. public variables
#10. private variables
#11. onready variables
onready var seven_follow := $Path2D/SevenFollow
onready var gwaj_follow := $Path2D/GwajFollow
onready var link_button := $LinkButton
onready var rehearsal := $LineSpokenTest
onready var tin7men := $Tin7Men
onready var ring_ship := $RingShip
onready var camera := $Camera2D
onready var fish_follow := $FishPath/FishFollow
#12. optional built-in virtual _init method
#13. built-in virtual _ready method
func _ready():
	tin7men.actions.frown.do()
	tin7men.enabled = true
	Story.connect("scene_started", self, "_on_Story_scene_started")	
	Story.connect("scene_ended", self, "_on_Story_scene_ended")
	for node in get_tree().get_nodes_in_group("Black"):
		var label = node as Label
		if label:
			label.add_color_override("font_color", Color(0,0,0,1))
	
#14. remaining built-in virtual methods
func _physics_process(delta):
	seven_follow.unit_offset += delta * follow_scale
	gwaj_follow.unit_offset += delta * follow_scale
	fish_follow.unit_offset += delta * follow_scale
	
	if Input.is_action_just_pressed("reset"):
		printt("Level03 reset!!!")
		if Jm.manager():
			Jm.events().raise_event("level_failed", { scene_id = "Level03" })
		else:
			push_warning("No manager...running without, again? ;-)")

#15. public methods
#16. private methods
func _on_LinkButton_pressed():
	var result = OS.shell_open(code_link)
	assert(OK == result)


func _on_Story_scene_started(_scene_name):
	rehearsal.visible = true


func _on_Story_scene_ended():
	rehearsal.visible = false


func _on_StartScene_pressed():
	Story.start_scene("Main")


func _on_RingShip_boarding_zone_entered(body):
	if body == tin7men and body.get_parent() == self:
		var remote_transform = tin7men.get_node("RemoteTransform2D")
		tin7men.remove_child(remote_transform)
		remove_child(tin7men)
		ring_ship.board(tin7men)
		ring_ship.gravity_scale = 1.0
		ring_ship.add_child(remote_transform)
#		#ring_ship.wrap_mode = ring_ship.WrapMode.WRAP

func _on_AreaRing_body_entered(body):
	if not body is TileMap:
		printt("level finished!!!", body)
		if Jm.manager():
			Jm.events().raise_event("level_finished", { scene_id = "Level03" })
		else:
			push_warning("No manager...running without, again? ;-)")
