extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

onready var tin7men = $Tin7Men

# Called when the node enters the scene tree for the first time.
func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	
	tin7men.enabled = true

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Tin7Men__propel(node, from, direction):
	add_child(node)
	node.rotation = direction
	node.position = from
	node.velocity = node.velocity.rotated(direction)
	printt("node velocity", node.velocity)
