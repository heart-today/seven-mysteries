# From https://docs.godotengine.org/en/stable/getting_started/scripting/gdscript/gdscript_styleguide.html#code-order
# 01. tool
tool
# 02. class_name
# 03. extends
extends Node2D
# 04. # docstring
# 
# 05. signals
signal _private() # warning if not wired, send only primitive data for network
signal public()
# 06. enums
# 07. constants
# 08. exported variables
export var dimension := 7
export var power := 1
export var solid := false
export var center := Vector2.ZERO
export var angle_from := 0
export var angle_to := 360
export var reverse_ring_colors := false
export var black_and_white := true
export var rebuild := false setget set_rebuild
# 09. public variables


# 10. private variables
var _rainbow_colors := [Color.red, Color.orange, Color.yellow, Color.green, Color.blue, Color.indigo, Color.violet]
# 11. onready variables
onready var body := $KinematicBody2D
onready var poly := $KinematicBody2D/Polygon2D
# 
# 12. optional built-in virtual _init method
# 13. built-in virtual _ready method

# Called when the node enters the scene tree for the first time.
func _ready():
	_build()
	
	var gradient = Gradient.new()
	
	for i in gradient.get_point_count():
		var offset = gradient.get_offset(i)
		var color = gradient.get_color(i)
		printt(i, offset, color)
		
	printt("colors", gradient.colors)
	printt("offsets", gradient.offsets)
	
	gradient.colors = PoolColorArray()
	gradient.offsets = PoolRealArray()
	
	var offset := 0.5
	for i in len(_rainbow_colors):
		var color = _rainbow_colors[i]
		gradient.add_point(offset, color)
		offset += 1
		printt(i, color)
	gradient.add_point(offset, _rainbow_colors[0])

		
	for i in len(_rainbow_colors) + 1:
		printt(i, gradient.interpolate(i))


# 14. remaining built-in virtual methods

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
   pass


func _get_configuration_warning() -> String:
	var big_warning = ""
	for warning in _get_configuration_warnings():
		big_warning += warning
	return big_warning

# 15. public methods
func set_rebuild(r):
	print("rebuild")
	update()
	
# 16. private methods
func _build():
	# Extract (ETL)
	var ring_colors := _rainbow_colors.duplicate()
	if black_and_white:
		ring_colors.push_front(Color.black)
		ring_colors.push_back(Color.white)
	if reverse_ring_colors:
		ring_colors.invert()

	var color_count = len(ring_colors)
	var point_count = dimension
	var radius = pow(dimension, power)
	var line_width = pow(dimension, power - (0 if solid else 1))

	var outer_points := PoolVector2Array()
	var inner_points := PoolVector2Array()
	var outer_colors := PoolColorArray()
	var inner_colors := PoolColorArray()
	
	# Transform (ETL)
	for i in range(point_count + 1):
		var color = ring_colors[i % color_count]
		outer_colors.push_back(color)
		if true: # if not solid:
			inner_colors.push_back(color)
		var angle_point = deg2rad(angle_from + i * (angle_to-angle_from) / point_count - 90)
		var angle_vector = Vector2(cos(angle_point), sin(angle_point))
		outer_points.push_back(center +  angle_vector * radius)
		if true: # not solid:
			inner_points.push_back(center +  angle_vector * (radius - line_width))
		
	if true: # not solid:
		inner_points.invert()
		outer_points.append_array(inner_points)
		inner_colors.invert()
		outer_colors.append_array(inner_colors)
	
	# Load (ETL)
	
	poly.polygon = outer_points
	poly.vertex_colors = outer_colors
	
func _get_configuration_warnings() -> Array:
	return _get_private_signal_warnings()

func _get_private_signals() -> Array:
	return ["_private"]

# Ensure self signals are connected to some node between parent and scene_root!
func _get_private_signal_warnings() -> Array:
	var warnings = []
	for ps in _get_private_signals():
		var connection_list = get_signal_connection_list(ps)
		if connection_list.empty():
		   warnings.append("Connect %s signal between parent and scene_root\n" % ps)
	return warnings


func _on_KinematicBody2D_mouse_entered():
	print("enter")


func _on_KinematicBody2D_mouse_exited():
	print("exit")
