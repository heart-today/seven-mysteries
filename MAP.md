# Map

## Aggie

[Online Starter Map](https://aggie.io/_iadwcxjd2)

Unfortunately, no artist at the moment...

## Overview

### SevenHaptHeartless7TinMan

The SevenHaptHeartless7TinMan stands inside the GoddessGawjHaptMap.

The map is the GoddessGawjHapt, in PartialRainbowHaptagon form, with 7 RainbowQox, each made up of 7 Qox.

However, the GoddessGawjHapt is flawed, so isn't a FullRainbowHaptagon.

Only one of ROGYBIV per RainbowQox, the rest are Black.

A Hapt has 49 clickable areas, but the GoddessGawjHapt has only 7 clickable areas.

However, there are really only 7 QoxLevels, determined by their Color.

At first, you can only click on the inner ring.

VoiceOver: You must roll 1D7 to choose your first Qox to enter.

The innermost Haptagon spins like The Price Is Right....

*clickety*

And now you can 