# MarkDown2Godot

A way to create games from a Markdown specification, implemented in Godot (and Python Mistune)

# Research

Research [MarkdownQuickRef](https://wordpress.com/support/markdown-quick-reference/)
* Block Quotes
> Use for [Narrator and/or VoiceOver](https://bunnystudio.com/blog/library/voice-over/the-difference-between-voice-over-vs-narration/#:~:text=The%20difference%20between%20narration%20vs,tends%20to%20be%20quite%20small.&text=The%20voice%2Dover%20is%20a,entire%20story%20to%20the%20audience.)

> See what happens in JSON file after mistune_story.py
* Abbreviations!!! HTML
  * Sadly, apparently not supported by Gitlab WebIde Preview Markdown
  * [HTML]: HyperText Markup Language
* WebIde requires backquote character for codeblocks, does Mistune?
  * FIND OUT!  Run MD2G.md through mistune_story.py!

# Implementation

* Mistune

# Characters

`{ "abbreviations": {} }`

# Stories

## MyStory
`{ "D": "Ducky", "LF": "LittleFoot"}`
### Act 1
#### Lila
#### Lila
`["D","LF"]` 
> Characters in MyStory/Act 1/Lila 2 `["D","LF"]` 

> Continue from previous character, which is first character **D** which is *Ducky* ... `code block in block quote?`

I'm hungry 
...maybe a burrito... 
> Ducky again

...or a 'burger...
> ...still Ducky...

`_` *sigh* 
> switches character, and *emphasized words* are _emotion_...*em4em*, see?

> In other words, *LittleFoot* emotes *sigh*

> Now switch back to *Ducky* explicitly...in case you got lost in all the hubbub of words... ;-)

`Ducky` What do you think? 
~~~
* 'character = "Ducky"'
* 'line = "What do you think?"
* 'character_line_index = 1'
* 'lila_line_index = 1'
* 'audio_path ~ "Story/MyStory/Act 1/Lila 2/little_foot_what_do_you_think?.wav'
* 'audio_path ~ "my_story_1_2_lf_1.ogg"
~~~
vs
```
{ 
  'character' = "Ducky",
  'line' = "What do you think?",
  'character_line_index' = 1,
  'lila_line_index' = 1,
  'audio_path' = "Story/MyStory/Act 1/Lila 2/little_foot_what_do_you_think?.wav',
  'audio_path' = "my_story_1_2_lf_1.ogg",
}
```
`_` I don't know. You? 
> LittleFoot says ...

`_something` Something really complicated that I don't want as a file name but needs to be said
> 'audio_path' = "Story/MyStory/Act 1/Lila 2/little_foot_something.wav"

> But might seem odd that the **_** at the front didn't switch characters...

> Remember `something` would try to switch character to *something*, and give a **WARNING** about no such character

> One way to think about it is that `_` means *Do()*, which **Lila2** interprets as "Ok...I'll switch characters"

> ...while `_something` means *Do(something)*, which **Lila2** interprets as "I'll set the audio label to *something*"

'Crackers' What about me?
> WARNING Introducing unknown character *Crackers@md2g://gitlab.com/heart-today/seven-mysteries/-/blob/master/MD2G.md#Stories/MyStory/Act%20/Lila:2/line:3'*

> See [Is Colon URL Safe? Is URL Colon-safe? ;-)](https://stackoverflow.com/questions/2053132/is-a-colon-safe-for-friendly-url-use)
