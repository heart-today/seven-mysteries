# SevenMysteries

[[_TOC_]]

## Welcome 7!

###

* 7? = 2
* 7Seven? : 7 letters 7 : [0, ROYGBIV, 1] = 3
* 7ToThe0 : 7    7    7 = 4
* 7 ^ 0 = 1 is 5 7 : One4All 04A&A41 All4One - 7 letters 7
* 0Their7Temples 2 7
* Hanumanji Yogi 2 7
* 7Seven(SacredLogos of 7Sevens and the GoddessGawj)
* Seven7(Paths of HaptHeartlessTinMen and LovingGawjerCompaltars)

The project for submission to [Godot Wild Jam #32](https://godotwildjam.com)

All are welcome!  

Join us in [Falling Leaves on Discord](https://discord.gg/vwS5bgvQgk)

## Demo

Continuing development [Demo for Seven Mysteries](http://theirtemple.com/web/seven-mysteries/seven-mysteries.html) 

Hosted on [Their Temple](http://theirtemple.com)


## Intentions

* To gratefully give and receive help
* Find wonderful people to work together & give togethe 7 r   (7whoopsy!)
* Breathe life into SevenHaptHeartless7Tinmen and friends
* Continually expand the [Demo](http://theirtemple.com/web/seven-mysteries/seven-mysteries.html)
* Deliver the [Story](https://gitlab.com/heart-today/seven-mysteries/-/blob/master/STORY.md) in a beautiful voice
* Joyfully implement the [Design](https://gitlab.com/heart-today/seven-mysteries/-/blob/master/DESIGN.md)
* Bring the [Map](https://gitlab.com/heart-today/seven-mysteries/-/blob/master/MAP.md) into the real
* Merge [Architecture](https://gitlab.com/heart-today/seven-mysteries/-/blob/master/ARCHITECTURE.md) and [Content](https://gitlab.com/heart-today/seven-mysteries/-/blob/master/CONTENT.md) inside of [Godot](https://godotengine.org) with grace and style
* Expand from our experience in the [Godot Wild Jam](https://godotwildjam.com)
* ...and have fun and spread joy!

## Features

## Installation

## Attributions

### Inspirations

* [Water-T And The Rise Of The Numbericons](https://www.youtube.com/watch?v=LPDn0PC6zFE)

### Code

### Tutorials

## License

MIT like Godot, please

Copyright 2021
* Your name here...
* Hanumanji Yogi 
