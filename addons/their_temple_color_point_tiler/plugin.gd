tool
extends EditorPlugin

#var Ring = preload("res://Scenes/Ring/Ring.gd")

func _enter_tree():
	# Initialization of the plugin goes here.
	# Add the new type with a name, a parent type, a script and an icon.
	add_custom_type("Ring", "Node2D", preload("Ring/Ring.gd"), preload("ring_icon.png"))
	add_custom_type("Tin7Men", "Node2D", preload("res://Scenes/Tin7Men/Tin7Men.gd"), preload("ring_icon.png"))
	pass
	
#func handles(object):
#	return object is Ring

func _exit_tree():
	# Clean-up of the plugin goes here.
	# Always remember to remove it from the engine when deactivated.
	remove_custom_type("Ring")
	remove_custom_type("Tin7Men")
	pass

