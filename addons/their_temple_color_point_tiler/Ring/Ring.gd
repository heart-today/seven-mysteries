# From https://docs.godotengine.org/en/stable/getting_started/scripting/gdscript/gdscript_styleguide.html#code-order
#01. tool
tool
#02. class_name
#03. extends
extends Node2D
#04. # docstring
# TODO Extend to start/end color, count and interpolation style 
#   var colors = Processors.color.interp(black, white, color.interp.enum.rainbow)
#   ie draw_multi_arc_wide(center, radius, colors, width)
#05. signals
signal build_started(config)
signal build_polygon_2d(polygon_2d)
signal build_collision_polygon_2d(collision_polygon_2d)
signal built()
#06. enums
#07. constants
const RING_N = "Ring%d"
#08. exported variables
export(int,1,7) var count := 7 setget _set_count
export var center := Vector2.ZERO setget _set_center
export var angle_to := 360 setget _set_angle_to
export var angle_from := 0 setget _set_angle_from
export var radius_start = 180.0 setget _set_radius_start
export var radius_delta = 5.0 setget _set_radius_delta
export var reverse_ring_colors = false setget _set_reverse_ring_colors
export var black_and_white = true setget _set_black_and_white
export var point_power = 1 setget _set_point_power 	# 7 to the PointPower!
export var animate := true
export var fps := 10 
export(int, FLAGS, "Red", "Orange", "Yellow", "Green", "Blue", "Indigo", "Violet") var active = 0

#09. public variables
var collision_object = null setget _set_collision_object
#10. private variables
var _anim_index = 0
var _ring_colors = [Color.black, Color.red, Color.orange, Color.yellow, Color.green, Color.blue, Color.indigo, Color.violet, Color.white]
var _ring_color_collision_polygons = {}
#11. onready variables
#12. optional built-in virtual _init method
#13. built-in virtual _ready method
func _ready():
	# TODO Make a theme?
	build()
	
var _accum := 0.0  # In delta seconds
func _process(delta):
	if animate:
		_accum += delta
		if _accum > 1.0/fps:			
			_anim_index = (_anim_index + 1) % count
			show(_anim_index + 1)
			_accum = 0
	else:
		show()
		
#14. remaining built-in virtual methods
func build(config = {}):
	var radius = radius_start
	# Since may invert and popoff a few...
	var ring_colors = _ring_colors.duplicate()
	if reverse_ring_colors:
		ring_colors.invert()
	if not black_and_white:
		ring_colors.pop_back()
		ring_colors.pop_front()
	
	# Remove all the previous polygons
	for child in get_children():
		remove_child(child)
		
	for i in 7:
		var ring_color = ring_colors[i]
		var polygon = _build_arc_wide(radius, ring_color)
		radius -= radius_delta
		polygon.name = RING_N % i
		add_child(polygon)
	show()
	
	_attach_collision_polygons()
	
	emit_signal("built")

	update()

func show(n = count):
	assert(n >= 0)
	assert(n <= count)
	for i in 7:
		var ring = get_node(RING_N % i)
		if ring:
			ring.visible = i < n
		else:
			push_error("WTF?  Where did the ring go?")
		
#15. public methods
#16. private methods
func _set_collision_object(co):
	collision_object = co
	_attach_collision_polygons()
	#print("WARNING No longer attaching collision polygons!!!")

func _attach_collision_polygons():
	if collision_object:
		for color in _ring_color_collision_polygons:
			collision_object.add_child(_ring_color_collision_polygons[color])
			
func _set_count(cnt):
	count = cnt
	show()
	
func _set_center(cntr):
	center = cntr
	build()
	
func _set_angle_to(at):
	angle_to = at
	build()
	
func _set_angle_from(af):
	angle_from = af
	build()

func _set_radius_start(rs):
	radius_start = rs
	build()
	
func _set_radius_delta(rd):
	radius_delta = rd
	build()
	
func _set_reverse_ring_colors(rrc):
	reverse_ring_colors = rrc
	build()
	
func _set_black_and_white(bnw):
	black_and_white = bnw
	build()
	
# 7 to the PointPower!
func _set_point_power(pp):
	if pp in [1, 2]:
		point_power = pp
		build()

func _get_configuration_warning():
	var w = ""
#	if not collision_object:
#		w = "Set the collision_object variable to CollisionObject2D to generate CollisionPolygon2D from ring shape"
	return w

# From https://docs.godotengine.org/en/stable/tutorials/2d/custom_drawing_in_2d.html
func _build_arc_wide(radius, color):
	var polygon_top := PoolVector2Array()
	var polygon_bottom := PoolVector2Array()
	var points_arc := PoolVector2Array()

	var point_count = int(pow	(7, point_power))
	
	for i in range(point_count + 1):
		var angle_point = deg2rad(angle_from + i * (angle_to-angle_from) / point_count - 90)
		var angle_vector = Vector2(cos(angle_point), sin(angle_point))
		polygon_top.push_back(center +  angle_vector * (radius + (radius_delta/2)))
		points_arc.push_back(center + angle_vector * radius)
		polygon_bottom.push_back(center +  angle_vector * (radius - (radius_delta/2)))

	#draw_polyline(points_arc, color, radius_delta)
	
	polygon_bottom.invert()
	polygon_top.append_array(polygon_bottom)
	
	var collision_polygon : CollisionPolygon2D = null
	if color in _ring_color_collision_polygons:
		collision_polygon = _ring_color_collision_polygons[color]
	else:
		collision_polygon = CollisionPolygon2D.new()
		_ring_color_collision_polygons[color] = collision_polygon


	collision_polygon.polygon = polygon_top
	# polygon = polygon_top
	
	var polygon = Polygon2D.new()
	polygon.polygon = polygon_top
	polygon.color = color
	#print(color)

	return polygon
