# Design

[[_TOC_]]

## Godot Wild Jam Theme and Wildcards

Seven is the Theme

Wildcards are:
* Good Things Come To Those Who Wait (WAIT) - Player Standing Idle Helps Their Progression
* Dare To Fail (FAIL) - Failure is just Another Path to Victory
* Roll a D20! (RPG) - Include some RPG mechanics in your game (stats, skills, etc)

## Game Building Roles Summary

## Game Playing Roles Summary

Sadly, there are not 7 possible, nor Seven7 roles...LOL

### Seeker (Vishnu):

#### Seven7(HaptHeartlessTinMen)

* You play Seven7HaptHeartlessTinMen (77HHTM), whose beautiful sacred HaptHeart has been torn away.
* The GoddessGawj and Seven7(GawjerCompaltars) have shattered the HaptHeart...
* and used the 7Sevens of Qox and Pix to build the GoddessGawjHapt.
* You enter the GoddessGawjHapt to fight the Seven7(GawjerCompaltars) (and their various minions)...
* Explore the maze of the GoddessGawjHapt...
* Collect 7Sevens of Qox and Pix
* Defeat 7ZEROES (another 7 letters!  Woot!), the MainMinion of GoddessGawj
* ...and find your HaptHeart whole again!

###  Builder (Brahma)

#### 7Seven(LovingGawjerCompaltars)

* As 7Seven(LovingCompaltars), you build Qox for the HaptHeart, to defend from Seven7(CravingGawjerCompaltars)
* Using CompaltarParts, Rainbow, Hapt, Qox, and Pix
* (...using Qox to build Qox, yes, we know that is confusing...hopefully, more will become clear!)

#### Seven7(GivingGawjerCompaltars)

* As Seven7(GivingGawjerCompaltars), you build Qox for the GoddessGawjHapt, to defend from Seven7(HaptHeartlessTinMen)
* Using CompaltarParts, Rainbow, Hapt, Qox, and Pix
* (...using Qox to build Qox, yes, we know that is confusing...hopefully, more will become clear!)

### Destroyer (Shiva)

#### 7Seven(CravingGawjerCompaltars)

* You play 7Seven(CravingGawjerCompaltars), followers of GoddessGawj
* Craving 7s, sevens, SEVENS, 7SEVENS (which is 7 letters!), ALL THE 7SEVENS!
* Break into the HaptHeart from outside
* Collect 7Sevens from within the HaptHeart (seven from each Qox)
* Bring All7Seven offerings...to Her, GoddessGawj!

## Seven Mysteries

Can you Humanize...a Digit?

Explore the Seven Mysteries:
* Who are you as 7?
* What is a 0?  And can it...actually...hurt?
* Where are the Numerals that came before you...1,2,3,4,5,6?
* When will you get back to Mathland?
* Why ???
* How ???
* How much ???

## Other Themes

* NumericalCompassion - Humanizing a Digit
* 7 8 9 - NumericalCannibalism...is it a thing?
* 7 Primary Actions - WASD/Arrows=JumpLeftDuckRight Space=Dash LeftClick=Throw Mouse=Aim
* 7 Toggle Keys - Numbers 1 - 6, and 0
  * 1 - 6 - Add which Point/BodyPart to Throw
  * 0/RightClick Reset Throw
* 7 Control Keys - Tab=Stats Screen Escape=Menu
* Math<->Computer - The Neverending Cycle! 
  * TrappedInComputer - a single digit, lost in a computer!
  * Help 7 find it's friends 1..6
  * Avoid the dreaded 0...the absence of number!
  * Look out for Operators - they can build you up, tear you down, and change your relationship to the world
    * Plus/Minus
    * Multiply/Divide
    * Iterate
    * Poke/Peek
* Rainbow - ROYGBIV colors
  * ColorPoints - used for Stats!
* Numerals - 1,2,3,4,5,6,7...and the dreaded 0!  Mwuahahaha!
* ConcentricRings - 7 of them, all over the place...Terrains as Rings!
* Representations - How to represent 7?  Polygon?  Points?  Differing colors?
  * PointCloud
    * Polygon (partially connected PointCloud)
    * Polygram (fully connected PointCloud)
  * CPUParticle2D
* BodyParts - 7 elements per NumericalCharacter to humanize them...2 hands, 2 feet, 2 body, 1 head/face = 7 parts!
  * Pick up BodyParts as you go along...NumericalCompassion!
  * See [Out of Power](https://mrbundles.itch.io/out-of-power) for how to humanize a Battery!
* [Who, What, Why, When, Where, How, How Much?](https://www.consultantsmind.com/2013/03/07/7-key-questions-who-what-why-when-where-how-how-much/)
* Coalescences (mostly for the Intro ;-)

## Story Outline

### Intro

* Use PointCloud...OnePoint, TwoPoints, ... ManyPoints, popping into existence.
* Coalescing, so many Points, solidifying, transforming between Points and Tiles
  * Building/unbuilding the Map
  * Then SevenPoints get...isolated, selected out, and placed on the still wavering Point/TileWorld
* Then into the Tutorial

## Mechanics

### PointPickup

* On Pickup, Point Count increases up to 7

### OperatorPickup

* (Optional) On Pickup, Operators Hover until Number Pressed, then Operator goes to that Point.

### ColorPickup

* (Optional)On Pickup, Colors Hover until Number Pressed, then Color goes to that Point.
* (Default)HighestColor goes to Slot1

### Throw

* Mouse Targeting - Head and Hands follow Mouse
* Press 1 - 6 to Select Operator (if Any)
* Left Click (and optional BodyPart and Operator) go forward!  +1

####  Example

* SlotsEmpty
* Pickup OperatorPlus, Slot1(OperatorPlus), Selected(Slot1)
* Pickup OperatorMinus, Slot2(OperatorMinus), Selected(Slot2)
* Press 1,  Selected(Slot1)
* Mouse Aim, LeftClick Throw Slot1
  * Sends +1 (OperatorPlus and OnePoint from NumeralPointCloud) Through The Air
  * Does ??? on ???
* Pickup Point
* Pickup ColorRed(1)
* Mouse Aim, LeftClick Throw Slot1

### RPG Stats

* Speed
* Dash
* Agility (for Powerups?)

## Characters

So far we have:
* 7 as PlayerCharacter
  * PointCloud
  * Finds BodyParts
* 1,2,3,4,5,6 as NPCs (Friends?)
* 0 as Boss (Enemy?)
* Operators as Pickups

### Representations

0. (Infinity?) ??? - Black/White
0. Point - Red/Violet
0. Line (Ray?) - Orange/Indigo
0. Triangle - Yellow/Blue
0. Square (TwistedSquare - Hourglass - ?) - Green
0. Pentagon (Pentagram) - Blue/Yellow
0. Hexagon (Hexagram) - Indigo/Orange
0. Heptagon (Heptagram) - Violet/Red

### Interactions

## Terrains

Seven TerrainTypes, mapping to the Rainbow, placed in rings.

* Red
* Orange
* Yellow
* Green
* Blue
* Indigo
* Violet

### Interactions


